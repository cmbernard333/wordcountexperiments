mod word_count;

use std::env;

/*Print newline, word, and byte counts for each FILE, 
 and a total line if more than one FILE is specified. 
 With no FILE, or when FILE is -, read standard input.
 
 Example:

 hello.txt : 
 hello, bob!

 wc hello.txt
 1  2 12 hello.txt

 */
fn main() {
    let args : Vec<String> = env::args().collect();
    let filename = &args[1];
    let wc = word_count::read_file(filename);
    println!("{} {} {}", wc.newline, wc.word, wc.bytes);
}
