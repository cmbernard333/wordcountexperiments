use std::io::{BufReader, BufRead};
use std::fs::File;

pub struct WordCount {
    pub newline : i32,
    pub word : i32,
    pub bytes : usize,
}

pub fn read_file(filename: &String) -> WordCount {
    let f = File::open(filename).expect("file not found");
    let mut filebufreader = BufReader::new(&f);
    let mut wc = WordCount { 
        newline : 0, 
        word: 0, 
        bytes : 0};
    let mut lc : char = ' ';
    let mut bufstring = String::new();

    // be careful here - expect may cause it to fail
    while filebufreader.read_line(&mut bufstring).expect("No fail") != 0 {
        let chars = bufstring.chars();

        // each time we get a new line we have an increment
        // KEEP IN MIND A '\n' MAY NOT EXIST
        wc.newline = wc.newline + 1;

        // count bytes
        wc.bytes = wc.bytes + bufstring.bytes().count();

        // read char by char 
        // increment char count unless
        // -> increment word count when char = space (unless tab)
        // --> don't iterate subsequent spaces - wait until next char
        // increment new line count when char = '\n'
        for c in chars {
            if c.is_whitespace() && !lc.is_whitespace() {
                wc.word = wc.word + 1;
            }
            lc = c
        }
        if !lc.is_whitespace() {
            wc.word = wc.word + 1
        }
    }
    return wc;
}