# Package

version       = "0.1.0"
author        = "Christian Bernard"
description   = "Word count utility in Nim"
license       = "MIT"
srcDir        = "src"
bin           = @["wordcount"]

# Dependencies

requires "nim >= 0.18.0", "docopt >= 0.6.6"

# Tests

task test, "Runs the test suite":
    exec "nim c -r tests/tester"
