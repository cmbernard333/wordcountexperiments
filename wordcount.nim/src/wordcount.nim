# This is a comment
import wordcountpkg/wc
import os

proc main() =
    #[ 
        this is fancy - if the paramCount > 0 - use paramStr to read the first argument
        and store in filename
    ]#
    let filename = if paramCount() > 0 : paramStr(1) else : ""
    var wc : WordCount = wordCountFile(filename)
    echo wc.newline," ", wc.word," ", wc.bytes," ", filename

main()