# wordCount module
import strutils
# * means export this from the module
# tuple is just a grouping of elements
type 
    WordCount* = tuple
        newline : uint
        word : uint
        bytes : int64

# this function is exported because of *
proc wordCountFile*(filename : string) : WordCount =
    var wc : WordCount
    var f : File = open(filename) # raises exception if the file named filename doesn't exist
    var lc = ' '

    # grab sizse of file
    wc.bytes = getFileSize(f)

    # iterate over all the chars
    for line in f.lines:
        wc.newline = wc.newline + 1
        for letter in line:
            if isSpaceAscii(letter) and not isSpaceAscii(lc):
                wc.word = wc.word + 1
            lc = letter

    close(f)
    result = wc # this is the return value - alot like pascal
