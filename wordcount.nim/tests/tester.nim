import wordcountpkg/wc as wc

#[
    Run all the unit tests
    define a new test in here and add it to the tests sequence
]#

proc testWordCountEmptyFile() =
    let filename = "res/empty.txt"
    var wordCount : wc.WordCount = wc.wordCountFile(filename)
    assert(wordCount.newLine == 1)
    assert(wordCount.word == 0)
    assert(wordCount.bytes == 2)

proc testWordCountSingleLine() =
    let filename = "res/hello.txt"
    var wordCount : wc.WordCount = wc.wordCountFile(filename)
    assert(wordCount.newLine == 1)
    assert(wordCount.word == 1)
    assert(wordCount.bytes == 13)

# sequence of tests - add test here to do all the testing
let tests = @[testWordCountEmptyFile, testWordCountSingleLine]

for test in tests:
    test()