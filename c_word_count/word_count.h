#pragma once

#include <stdlib.h>

struct word_count_t {
	unsigned long int newline;
	unsigned long int word;
	size_t bytes;
};

int word_count_file(struct word_count_t* wc, const char* filename);