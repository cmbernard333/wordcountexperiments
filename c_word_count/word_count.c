#include "word_count.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int word_count_file(struct word_count_t* wc, const char* filename)
{
	FILE* pfile = NULL;
	char buff[128];
	unsigned char lc = ' ';
	size_t sz = 0;

	pfile = fopen(filename, "r");

	if (pfile == NULL)
	{
		perror("Error opening file");
		return errno; // file not found
	}

	// get file size
	fseek(pfile,0L,SEEK_END);
	sz = ftell(pfile);
	fseek(pfile,0L,SEEK_SET);
	wc->bytes = sz;

	// fgets fetchs 128 chars into a buffer
	// count newline char as new line (even on windows)
	// use previous char to get spaces to count words
	// count element of buff as a byte
	while (fgets(buff, 128, pfile) != NULL)
	{
		for(char* str = buff; *str != '\0'; str++)
		{
			char c = *str;
			// printf("char=%d\n",c);
			if (isspace(c) && !isspace(lc))
			{
				wc->word++;
			}
			if (c == '\n') 
			{
				wc->newline++;
			}
			lc = c;
		}
	}
	fclose(pfile);
	return 0;
}