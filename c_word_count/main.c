#include <stdio.h>
#include "word_count.h"

int main(int argc, char** argv) {

    int rc = 0;
    char* filename = NULL;
    struct word_count_t wc = {0};

    if (argc < 2) {
        fprintf(stderr,"Usage: %s [file]\n",argv[0]);
        return 1;
    }

    filename = argv[1];
    rc = word_count_file(&wc, filename);

    fprintf(stdout,"%ld %ld %ld %s", wc.newline, wc.word, wc.bytes, filename);
    return rc;
}